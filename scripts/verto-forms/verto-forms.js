﻿function ValidateVertoForm(FormID, SubmitID) {

    // Retrieve the calling form
    var vertoForm = document.getElementById(FormID);

    // Retrieve all inputs in the calling form
    var rawInputs = vertoForm.getElementsByTagName('input');
    var rawTextareas = vertoForm.getElementsByTagName('textarea');

    var vertoInputs = [];
    var vertoSelects = vertoForm.getElementsByTagName('select');

    // Add inputs to array
    for (i = 0; i < rawInputs.length; i++) {
        vertoInputs.push(rawInputs[i]);
    }

    // Add textareas to array
    for (i = 0; i < rawTextareas.length; i++) {
        vertoInputs.push(rawTextareas[i]);
    }

    // Assume form is valid
    var valid = true;

    // Store collection of failed input validations
    var failedInputs = [];
    var confirmFailedInputs = [];

    // Loop through all inputs in the form
    for (i = 0; i < vertoInputs.length; i++) {

        // Retrieve the validation message holder for the input
        var validationMessage = document.querySelector('span[data-input=' + vertoInputs[i].id + ']');

        if (validationMessage !== null) {

            // Hide the message
            validationMessage.setAttribute('style', 'display:none;');

        }

        var requiredMessage = document.querySelector('span[data-vinput=' + vertoInputs[i].id + ']');

        if (requiredMessage !== null) {
            
            requiredMessage.setAttribute('style', 'display:none;');

        }

        // Remove fail/success classes from the input if present
        var failClass = vertoInputs[i].getAttribute('data-fclass');
        var successClass = vertoInputs[i].getAttribute('data-sclass');

        VF_RemoveClass(vertoInputs[i], failClass);
        VF_RemoveClass(vertoInputs[i], successClass);

        var value = vertoInputs[i].value;

        // Check if the input has a pattern specified
        if (vertoInputs[i].getAttribute("pattern") !== null && vertoInputs[i].getAttribute("pattern").trim() !== '') {
            // Create a RegExp object from the pattern
            var regEx = new RegExp(vertoInputs[i].getAttribute("pattern"));

            // Ensure there is a value
            if (!VF_IsEmpty(value)) {

                // Perform validation
                if (!regEx.test(value)) {

                    // Failed to validate
                    valid = false;

                    // Add the failed input to the collection
                    failedInputs.push(vertoInputs[i]);

                }
                else {

                    VF_ApplyClass(vertoInputs[i], successClass);

                }

            }
        }

        // Check if the field needs to be confirmed
        if (valid && vertoInputs[i].hasAttribute('data-confirm')) {

            // Attempt to retrieve the confirm field
            var confirmField = document.getElementById(vertoInputs[i].getAttribute('data-confirm'));

            if (confirmField != null) {

                if (confirmField.value != value) {

                    valid = false;
                    confirmFailedInputs.push(confirmField);

                }

            }
        }

        if (vertoInputs[i].getAttribute('data-optional') === 'true') {

            // Optional; empty values allowed
            VF_ApplyClass(vertoInputs[i], successClass);

        }
        else {

            if (vertoInputs[i].type == 'checkbox')
            {
                if (vertoInputs[i].checked == false) {
                    
                    // This checkbox must be ticked
                    valid = false;

                    // Add the failed input to the collection
                    failedInputs.push(vertoInputs[i]);

                }
            }
            else
            {
                if (VF_IsEmpty(value)) {

                    // Empty values not allowed
                    valid = false;

                    // Add the failed input to the collection
                    failedInputs.push(vertoInputs[i]);

                }
                else {

                    if (!failedInputs.indexOf(vertoInputs[i]) > -1) {

                        VF_RemoveClass(vertoInputs[i], failClass);
                        VF_ApplyClass(vertoInputs[i], successClass);

                    }

                }
            }
        }
    }

    // Loop through all selects in the form
    for (i = 0; i < vertoSelects.length; i++) {

        // Remove fail/success classes from the input if present
        var failClass = vertoSelects[i].getAttribute('data-fclass');
        var successClass = vertoSelects[i].getAttribute('data-sclass');

        VF_RemoveClass(vertoSelects[i], failClass);
        VF_RemoveClass(vertoSelects[i], successClass);

        var requiredMessage = document.querySelector('span[data-vinput=' + vertoSelects[i].id + ']');

        if (requiredMessage !== null) {

            requiredMessage.setAttribute('style', 'display:none;');

        }

        if (vertoSelects[i].getAttribute('data-optional') === 'true') {

            // Optional; empty values allowed
            VF_ApplyClass(vertoSelects[i], successClass);

        }
        else {

            if (VF_IsEmpty(vertoSelects[i].value) || vertoSelects[i].value == '0') {

                // Empty values not allowed
                valid = false;

                // Add the failed input to the collection
                failedInputs.push(vertoSelects[i]);

            }
            else
            {
                // Optional; empty values allowed
                VF_ApplyClass(vertoSelects[i], successClass);
            }

        }

    }

    if (valid) {

        // Trigger the postback
        __doPostBack(SubmitID, '');

    }
    else {

        // Loop through all failed inputs
        for (i = 0; i < failedInputs.length; i++) {

            // Retrieve the validation message holder for the input
            var validationMessage = document.querySelector('span[data-input=' + failedInputs[i].id + ']');

            // Check if the field is optional and missing a value
            if (failedInputs[i].getAttribute('data-optional') !== 'true' && (VF_IsEmpty(failedInputs[i].value) || (failedInputs[i].tagName.toLowerCase() == 'select' && failedInputs[i].value == '0') || (failedInputs[i].type == 'checkbox' && failedInputs[i].checked == false))) {

                var requiredMessage = document.querySelector('span[data-vinput=' + failedInputs[i].id + ']');

                if (requiredMessage != null) {
                    requiredMessage.setAttribute('style', 'display:' + requiredMessage.getAttribute('data-display') + ';');                    
                }

            }

            if (validationMessage != null && !validationMessage.hasAttribute('data-is-confirm')) {

                // Display the message
                validationMessage.setAttribute('style', 'display:' + validationMessage.getAttribute('data-display') + ';');

                if (validationMessage.innerText == '') {
                    validationMessage.innerText = 'This field is required.';
                }

            }

            // Apply the failed class
            VF_ApplyClass(failedInputs[i], failedInputs[i].getAttribute('data-fclass'));

        }

        // Loop through all failed confirmation fields
        for (i = 0; i < confirmFailedInputs.length; i++) {

            // Retrieve the validation message holder for the input
            var validationMessage = document.querySelector('span[data-input=' + confirmFailedInputs[i].id + ']');

            if (validationMessage != null && validationMessage.hasAttribute('data-is-confirm')) {

                // Apply the failed class
                VF_ApplyClass(confirmFailedInputs[i], confirmFailedInputs[i].getAttribute('data-fclass'));

                // Display the message
                validationMessage.setAttribute('style', 'display:' + validationMessage.getAttribute('data-display') + ';');

            }

        }

        return false;
    }
}

function VF_IsEmpty(value) {

    return !(value != null && typeof value !== 'undefined' && value.trim() !== '');
}

function VF_RemoveClass(control, className) {

    if (className != null && className.trim() !== '') {

        if (control.classList.contains(className)) {
            control.classList.remove(className);
        }

    }

}

function VF_ApplyClass(control, className) {

    if (className != null && className.trim() !== '') {

        if (!control.classList.contains(className)) {
            control.classList.add(className);
        }

    }

}