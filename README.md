﻿# Verto Forms
**Forms that don't make you want to punch things.**

Version 1.0 (beta 7)

## Installing

1. Download Verto Forms and copy the folder structure (excluding README.md) to your project's root directory.
2. Add the following line to the *System.Web > Pages > Controls* in your *Web.config* (look for Utils if you can't find it): ```<add tagPrefix="VertoForms" namespace="VertoForms" />```

## Building a Verto Form

The VertoForm control is just a Panel with some extra features, so use it as such:

```aspx-cs
<VertoForms:VertoForm runat="server" ID="TestForm" DefaultButton="SubmitButton">
   
    <h2>Contact Us</h2>

    <asp:Button runat="server" ID="SubmitButton" Text="Submit" />

</VertoForms:VertoForm>
```

In order to add fields to your form, you must use the **VertoInput** control. This is a versatile control which allows for easy server-side validation. **Tel** and **Email** VertoInput controls currently have built-in server-side validation, with more to come, but you can easily add your own validation methods (see the *Custom Validation Methods* section).

You can also set the **Label** property of your VertoInput to automatically render an HTML label element. There are a multitude of other properties available on the control to affect the rendering of the label (amongst other features), such as **LabelAfter** and **LabelCssClass**, but the best way to explore these is to look at the class code (every property is explained). Without further ado:

```aspx-cs
<VertoForms:VertoForm runat="server" ID="TestForm" DefaultButton="SubmitButton">
   
    <h2>Contact Us</h2>

    <VertoForms:VertoInput runat="server" ID="ContactName" Label="Full Name"/><br>

    <VertoForms:VertoInput runat="server" ID="ContactEmail" Label="Email Address" Type="Email"/>

    <VertoForms:VertoInput runat="server" ID="ContactPhone" Label="Phone Number" Type="Tel"/>

    <VertoForms:VertoInput runat="server" ID="ContactMessage" Label="Message" Type="Multiline"/>

    <asp:Button runat="server" ID="SubmitButton" Text="Submit" />

</VertoForms:VertoForm>
```

You now have a complete form, ready to submit.

## Handling Form Submission

Form submissions can be triggered by clicking on the VertoForm's DefaultButton control (same as a standard Panel), or they can be triggered from the code behind by calling ```TestForm.Submit()```.

When the form is submitted, the VertoForm.Submitted event is raised. This includes a SubmittedEventArgs object which includes a success boolean (**e.Success**), and an array of VertoInput controls which failed to validate (**e.FailedInputs**), if any.

```csharp
protected void TestForm_Submitted( object sender, VertoForms.SubmittedEventArgs e )
{
    if( e.Success )
    {
        // Form validated and submitted successfully; do what you like here
    }
    else
    {
        // Form failed validation; let's loop through the failed inputs
        foreach( var input in e.FailedInputs )
        {
            Response.Write( "The following value is invalid: " + input.Value + "<br>" );
        }
    }        
}
```

## Emailing the Form

"How do I use the Mailer class with VertoForms?" You don't. VertoForms doesn't need friends. VertoForms has a mail function of its own; just call ```TestForm.Mail()``` with any additional parameters you need (check IntelliSense):

```csharp
protected void TestForm_Submitted( object sender, VertoForms.SubmittedEventArgs e )
{
    if( e.Success )
    {
        TestForm.Mail( new[] { "cameron@vertouk.com" } );
    }
}
```

## Clearing the Form (or not)

By default, a VertoForm will clear all its VertoInput controls' values upon successful submission. If you want to prevent this behaviour, set **ClearOnSuccess** to **false** on your VertoForm control. If you would like to clear the values manually, call ```TestForm.Clear()``` from the code behind. You can also optionally include a list of VertoInput IDs to exclude, if there are any you would not like to clear.

## Custom Validation Methods

The static VertoValidation class powers both server-side and client-side validation for VertoForms. It includes predefined methods for validating human names, phone numbers and email addresses, but you can easily add your own via the ```VertoValidation.AddType()``` method. Currently you can define a RegEx for validation, or your own custom callback function.

**RegEx Example:**
```csharp
// Only allows a single word beginning with a capital letter.
// e.g. "Cameron" would be valid, but not "cameron", nor "Cameron Thomas".
VertoValidation.AddType( "Test", RegEx: "^([A-Z])\\w+$", IgnoreCase: false );
```

By default, empty strings are disallowed and validation is case-insensitive, but the ```AddType()``` method contains parameters which allow you to override this.

If you would like to have more fine-grain control over the RegEx, or not use RegEx at all, you can define your own callback function and add that instead. Your callback function *must* be a ```public static bool``` which accepts a single string as its parameter.

Note that callback functions do not currently support automatic client-side validation.

**Callback Example:**
```csharp
public partial class MyPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // Fails to validate if the input contains the word "goof"
        VertoValidation.AddType( "GoofCheck", ValidationType.Mode.Callback, Callback: MyValidationMethod );
    }

    public static bool MyValidationMethod( string Input )
    {
        if( Input.Contains("goof") )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
```

Once you have defined your own validation types, you can use them by adding ```ValidateMethod="TypeName"``` to your VertoInput control, e.g.:

```aspx-cs
<VertoForms:VertoInput runat="server" ID="FirstName" Label="First Name" ValidateMethod="Test"/><br>

<VertoForms:VertoInput runat="server" ID="Message" Label="Message" ValidateMethod="GoofCheck"/><br>
```

Everything else is handled for you; VertoForms will magically find your custom validation type and use it on form submission (assuming you have not goofed your spelling). If you name your validation type after a VertoInput type, e.g. "DateTime" or "Number", VertoForms will automatically use it to validate VertoInputs of that type, without having to add the ValidateMethod attribute.

If you want to use your validation manually, just call the ```Validate()``` method, e.g. ```VertoValidation.Validate( "My input string", "GoofCheck" )```.

## Password Validation

Password fields may be validated, but due to the varying requirements in terms of password strength, the password validation system is slightly different to other fields.

To add a password field to your VertoForm, simply the set the ```Type``` of your ```VertoInput``` to ```Password```.

By default, the VertoValidation class includes the following built-in password requirements:

* Basic - 4 characters minimum.
* Standard (default) - 8 characters minimum.
* Strong - 12 characters minimum, must contain at least 1 number.
* Extra Strong - 16 characters minimum, must contain at least 1 number and 1 special character.

Similar to custom validation methods, you can add your own password requirements by calling ```VertoValidation.AddPasswordRequirements()```, like below:

```csharp
VertoValidation.AddPasswordRequirements( "Bulletproof", MinLength: 64, NumbersRequired: 10, SpecialCharsRequired: 10 );
```

There is a user-facing ```Description``` property which can be set, as with ValidationTypes, but if you do not set this then one will be automatically generated for you.

Once you have defined your custom password requirements, you can use them by setting ```PasswordRequirements``` on your VertoInput to the name of your requirements, like so:

```aspx-cs
<VertoForms:VertoInput runat="server" ID="PasswordTest" Label="Password" PasswordRequirements="Bulletproof" Type="Password" />
```

The rest is handled automagically, both client-side and server-side, just like other validation.

## Confirm Fields

Some fields, such as passwords and email addresses, require a user to repeat their input to ensure there are no mistakes. In other words, you tend to have a "Password" field followed by a "Confirm Password" field, and the user has to type it out twice. This can be handled in VertoForms by simply setting the ```ConfirmField``` property on your VertoInput to the ID of the VertoInput which you would like to use for confirmation:

```aspx-cs
<VertoForms:VertoInput runat="server" ID="Password" Label="Password" Type="Password" ConfirmField="ConfirmPassword" />

<VertoForms:VertoInput runat="server" ID="ConfirmPassword" Label="Confirm Password" Type="Password" />
```

VertoForms will then validate the main password field as usual, then ensure that both fields match up (both client and server-side) and notify the user as appropriate.