﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

// Verto Forms
// Version: 1.0b8
// Author: Cameron Thomas
namespace VertoForms
{
    /// <summary>
    /// A form which uses VertoInput controls for validation.
    /// </summary>
    public class VertoForm : Panel
    {
        /// <summary>
        /// Event raised when the form is submitted (regardless of success).
        /// </summary>
        public event SubmittedEventHandler Submitted;

        /// <summary>
        /// The control which has been assigned as the DefaultButton for the form (if any).
        /// </summary>
        protected Control SubmitButton;

        private bool clearOnSuccess = true;

        /// <summary>
        /// Clear the form values automatically upon successful submission.
        /// </summary>
        public bool ClearOnSuccess
        {
            get
            {
                return clearOnSuccess;
            }

            set
            {
                clearOnSuccess = value;
            }
        }

        protected override void OnLoad( EventArgs e )
        {
            // Enable ViewState to retain values
            EnableViewState = true;

            // Check if a submit button's ID has been provided
            if( !String.IsNullOrWhiteSpace( DefaultButton ) )
            {
                // Attempt to retrieve the submit button using the ID
                Control submitBtn = FindControl( DefaultButton );

                // Check we found the submit button
                if( submitBtn != null )
                {
                    // Set the OnClientClick
                    submitBtn.GetType().GetProperty( "OnClientClick" ).SetValue( submitBtn, "ValidateVertoForm('" + ClientID + "', '" + submitBtn.UniqueID + "'); return false;" );

                    // Store the submit button
                    SubmitButton = submitBtn;

                    // Attempt to retrieve Click event for the submit button
                    EventInfo buttonEventInfo = SubmitButton.GetType().GetEvent( "Click" );

                    // Check the Click event exists
                    if( buttonEventInfo != null )
                    {
                        // Get MethodInfo for our internal Submit event
                        MethodInfo submitMethodInfo = GetType().GetMethod( "SubmitInternal", BindingFlags.NonPublic | BindingFlags.Instance );

                        // Create a delegate handler for the event
                        Delegate clickHandler = Delegate.CreateDelegate( buttonEventInfo.EventHandlerType, this, submitMethodInfo );

                        // Assign the delegate handler to the button's Click event
                        buttonEventInfo.AddEventHandler( SubmitButton, clickHandler );
                    }
                }
            }

            if( File.Exists( HttpContext.Current.Server.MapPath( "/scripts/verto-forms/verto-forms.min.js" ) ) )
            {
                // Register the validation JavaScript if the file is present (otherwise assume the dev will include it manually)
                ScriptManager.RegisterClientScriptInclude( this, GetType(), "VertoForms", "/scripts/verto-forms/verto-forms.min.js" );
            }

            base.OnLoad( e );
        }

        /// <summary>
        /// Validates and submits the form.
        /// </summary>
        public void Submit()
        {
            SubmitInternal( this, EventArgs.Empty );
        }

        /// <summary>
        /// Clears the values of all inputs in the control, with the option to exclude specific controls based on ID.
        /// </summary>
        /// <param name="ExcludedControls">A list of control IDs to exclude.</param>
        public void Clear( params string[] ExcludedControls )
        {
            foreach( VertoInput childControl in Controls.OfType<VertoInput>() )
            {
                // Check if the control is excluded
                if( !ExcludedControls.Contains( childControl.ID ) )
                {
                    // Not excluded; clear the value
                    childControl.Clear();
                }
            }

            foreach( VertoSelect childControl in Controls.OfType<VertoSelect>() )
            {
                // Check if the control is excluded
                if( !ExcludedControls.Contains( childControl.ID ) )
                {
                    // Not excluded; clear the value
                    childControl.Clear();
                }
            }
        }

        /// <summary>
        /// Fired when the form is submitted (either programmatically or via the DefaultButton). Validates the form.
        /// </summary>
        protected void SubmitInternal( object sender, EventArgs e )
        {
            // Prepare EventArgs for the VertoForm.Submitted event
            SubmittedEventArgs submitArgs = new SubmittedEventArgs();

            // Loop through all VertoInput child controls
            foreach( VertoInput childControl in Controls.OfType<VertoInput>() )
            {
                // Ensure required fields have a value
                if( !childControl.Optional && String.IsNullOrWhiteSpace( childControl.Value ) )
                {
                    submitArgs.Success = false;
                    submitArgs.FailedInputs.Add( childControl );
                    childControl.FailedMessage = "This field is required.";
                }
                else if( childControl.Type != VertoInput.InputType.Password )
                {
                    // Check if a custom validation method has been specified
                    if( !String.IsNullOrWhiteSpace( childControl.ValidateMethod ) )
                    {
                        // Attempt to validate with the specified type name
                        if( !VertoValidation.Validate( childControl.Value, childControl.ValidateMethod, childControl.Optional ) )
                        {
                            // Mark the submission as failed
                            submitArgs.Success = false;

                            // Add the failure message to the control
                            childControl.FailedMessage = VertoValidation.GetValidationType( childControl.ValidateMethod ).Description;

                            // Add the failed control to the collection
                            submitArgs.FailedInputs.Add( childControl );
                        }
                    }
                    else
                    {
                        // Check if a ValidationType exists for the field type
                        if( VertoValidation.GetValidationType( childControl.Type.ToString() ) != null )
                        {
                            // Attempt to validate the field
                            if( !VertoValidation.Validate( childControl.Value, childControl.Type.ToString(), childControl.Optional ) )
                            {
                                // Mark the submission as failed
                                submitArgs.Success = false;

                                // Add the failure message to the control
                                childControl.FailedMessage = VertoValidation.GetValidationType( childControl.Type.ToString() ).Description;

                                // Add the failed control to the collection
                                submitArgs.FailedInputs.Add( childControl );
                            }
                        }
                    }
                }
                else
                {
                    // Check if a PasswordRequirements name has been specified
                    if( !String.IsNullOrWhiteSpace( childControl.PasswordRequirements ) )
                    {
                        // Check specified PasswordRequirements name exists
                        if( VertoValidation.GetPasswordRequirements( childControl.PasswordRequirements ) != null )
                        {
                            // Validate using specified requirements
                            if( !VertoValidation.ValidatePassword( childControl.Value, childControl.PasswordRequirements ) )
                            {
                                // Mark the submission as failed
                                submitArgs.Success = false;

                                // Add the failure message to the control                                
                                childControl.FailedMessage = VertoValidation.GetPasswordRequirements( childControl.PasswordRequirements ).Description;

                                // Add the failed control to the collection
                                submitArgs.FailedInputs.Add( childControl );
                            }

                        }
                        else
                        {
                            throw new MissingPasswordRequirementsException( "A PasswordRequirements object with the name " + childControl.PasswordRequirements + " does not exist." );
                        }
                    }
                    else
                    {
                        // Validate using default requirements
                        if( !VertoValidation.ValidatePassword( childControl.Value, "Standard" ) )
                        {
                            // Mark the submission as failed
                            submitArgs.Success = false;

                            // Add the failure message to the control
                            childControl.FailedMessage = VertoValidation.GetPasswordRequirements( "Standard" ).Description;

                            // Add the failed control to the collection
                            submitArgs.FailedInputs.Add( childControl );
                        }
                    }
                }

                if( !String.IsNullOrWhiteSpace( childControl.ConfirmField ) )
                {
                    VertoInput confirmInput = Controls.OfType<VertoInput>().Where( input => input.ID == childControl.ConfirmField ).FirstOrDefault();

                    if( confirmInput != null )
                    {
                        if( confirmInput.Value != childControl.Value )
                        {
                            // Mark the submission as failed
                            submitArgs.Success = false;

                            // Display message for both controls
                            childControl.FailedMessage = "Please ensure both fields match.";
                            confirmInput.FailedMessage = "Please ensure both fields match.";

                            // Add both controls to failed inputs
                            submitArgs.FailedInputs.Add( childControl );
                            submitArgs.FailedInputs.Add( confirmInput );
                        }
                    }
                }
            }

            // Validation for <select> controls is much simpler
            foreach( VertoSelect childControl in Controls.OfType<VertoSelect>() )
            {
                // Check if the field is required and has a valid, non-zero and non-empty value selected
                if( !childControl.Optional && ( String.IsNullOrWhiteSpace( childControl.SelectedValue ) || childControl.SelectedValue == "0" ) )
                {
                    submitArgs.Success = false;
                    submitArgs.FailedSelects.Add( childControl );
                    childControl.FailedMessage = "This field is required.";
                }
            }

            // Validation for checkboxes is simpler still
            foreach( VertoCheckBox childControl in Controls.OfType<VertoCheckBox>() )
            {
                // Check if the field is required and has a valid, non-zero and non-empty value selected
                if( !childControl.Optional && childControl.Checked == false )
                {
                    submitArgs.Success = false;
                    submitArgs.FailedCheckBoxes.Add( childControl );
                    childControl.FailedMessage = "This field is required.";
                }
            }

            // Check if event handler exists for Submitted event
            if( Submitted != null )
            {
                // Raise the Submitted event
                Submitted( this, submitArgs );
            }

            // Clear the form values if necessary
            if( submitArgs.Success && ClearOnSuccess )
            {
                Clear();
            }
        }

        /// <summary>
        /// Sends the form contents as an email.
        /// </summary>
        /// <param name="Recipients">Array of email addresses to send the email to.</param>
        /// <param name="Subject">Subject line of the email.</param>
        /// <param name="Message">Message to display at the top of the email.</param>
        /// <param name="From">Email address to display as the sender of the email.</param>
        /// <param name="FromName">Name of the sender of the email.</param>
        /// <param name="ReplyTo">Array of email addresses to include in the "to" field when the email is replied to.</param>
        /// <param name="CC">Array of email addresses to receive a carbon copy of the email.</param>
        /// <param name="ExcludedFields">VertoInput IDs to exclude from the email.</param>
        public void Mail( string[] Recipients, string Subject = "Contact Form Submission", string Message = "", string From = "noreply@vertouk.com", string FromName = "", string[] ReplyTo = null, string[] CC = null, params string[] ExcludedFields )
        {
            // Prepare the email message
            string emailOutput = "";

            // Check if we have a message
            if( !String.IsNullOrWhiteSpace( Message ) )
            {
                // Append the message
                emailOutput += Message + "<br><br>";
            }

            foreach( VertoInput childControl in Controls.OfType<VertoInput>().OrderBy( i => i.Order ) )
            {
                // Only proceed if control has not been excluded
                if( !ExcludedFields.Contains( childControl.ID ) )
                {
                    // Ensure there is a value to retrieve
                    if( !String.IsNullOrWhiteSpace( childControl.Value ) )
                    {
                        // The field name to display in the email
                        string fieldName = "";

                        // Let's try the options for the name...
                        if( !String.IsNullOrWhiteSpace( childControl.Name ) )
                        {
                            fieldName = childControl.Name;
                        }
                        else if( !String.IsNullOrWhiteSpace( childControl.Label ) )
                        {
                            fieldName = childControl.Label;
                        }
                        else if( !String.IsNullOrWhiteSpace( childControl.ID ) )
                        {
                            fieldName = childControl.ID;
                        }

                        // We must have a name by now; add the field to the email
                        emailOutput += "<strong>" + fieldName + ":</strong> " + childControl.Value + "<br>";
                    }
                }
            }

            foreach( VertoSelect childControl in Controls.OfType<VertoSelect>().OrderBy( s => s.Order ) )
            {
                // Only proceed if control has not been excluded
                if( !ExcludedFields.Contains( childControl.ID ) )
                {
                    // The field name to display in the email
                    string fieldName = "";

                    // Let's try the options for the name...
                    if( !String.IsNullOrWhiteSpace( childControl.Name ) )
                    {
                        fieldName = childControl.Name;
                    }
                    else if( !String.IsNullOrWhiteSpace( childControl.ToolTip ) )
                    {
                        fieldName = childControl.ToolTip;
                    }
                    else if( !String.IsNullOrWhiteSpace( childControl.ID ) )
                    {
                        fieldName = childControl.ID;
                    }

                    // We must have a name by now; add the field to the email
                    emailOutput += "<strong>" + fieldName + ":</strong> " + childControl.SelectedValue + "<br>";
                }
            }

            foreach( VertoCheckBox childControl in Controls.OfType<VertoCheckBox>().OrderBy( c => c.Order ) )
            {
                // Only proceed if control has not been excluded
                if( !ExcludedFields.Contains( childControl.ID ) )
                {
                    // The field name to display in the email
                    string fieldName = "";

                    // Let's try the options for the name...
                    if( !String.IsNullOrWhiteSpace( childControl.Name ) )
                    {
                        fieldName = childControl.Name;
                    }
                    else if( !String.IsNullOrWhiteSpace( childControl.ToolTip ) )
                    {
                        fieldName = childControl.ToolTip;
                    }
                    else if( !String.IsNullOrWhiteSpace( childControl.ID ) )
                    {
                        fieldName = childControl.ID;
                    }

                    // We must have a name by now; add the field to the email
                    emailOutput += "<strong>" + fieldName + ":</strong> " + childControl.Checked.ToString() + "<br>";
                }
            }

            // Prepare optional parameters
            if( ReplyTo == null ) ReplyTo = new string[0];
            if( CC == null ) CC = new string[0];

            // Prepare SMTP email connection
            using( var emailClient = new SmtpClient( "mail.netdash.net" ) )
            {
                // Prepare the message
                MailMessage emailMessage = new MailMessage()
                {
                    From = new MailAddress( From, FromName ),
                    Body = emailOutput,
                    IsBodyHtml = true,
                    Subject = Subject
                };

                // Add all ReplyTo addresses to the message
                foreach( string replyToAddress in ReplyTo )
                {
                    emailMessage.ReplyToList.Add( replyToAddress );
                }

                // Add all recipients to the message
                foreach( string recipient in Recipients )
                {
                    emailMessage.To.Add( recipient );
                }

                // Add all carbon copy recipients to the message
                foreach( string ccRecipient in CC )
                {
                    emailMessage.CC.Add( ccRecipient );
                }

                // All done
                emailClient.Send( emailMessage );
            }
        }

    }

    /// <summary>
    /// Provides data for the VertoForm.Submitted event. 
    /// </summary>
    public class SubmittedEventArgs : EventArgs
    {
        /// <summary>
        /// Whether the form was submitted successfully.
        /// </summary>
        public bool Success = true;

        /// <summary>
        /// A collection of VertoInputs which have failed validation, alongside their failure messages.
        /// </summary>
        public List<VertoInput> FailedInputs = new List<VertoInput>();

        /// <summary>
        /// A collection of required VertoSelects which do not have a value selected.
        /// </summary>
        public List<VertoSelect> FailedSelects = new List<VertoSelect>();

        /// <summary>
        /// A collection of required VertoCheckBoxes which have not been checked.
        /// </summary>
        public List<VertoCheckBox> FailedCheckBoxes = new List<VertoCheckBox>();
    }

    /// <summary>
    /// Represents the method that will handle the VertoForm.Submitted event of a VertoForm control.
    /// </summary>
    public delegate void SubmittedEventHandler( object sender, SubmittedEventArgs e );

    /// <summary>
    /// Input control for use in a VertoForm.
    /// </summary>
    [DefaultProperty( "Value" )]
    public class VertoInput : WebControl, IPostBackDataHandler
    {
        /// <summary>
        /// Event raised when the input value is changed.
        /// </summary>
        public event EventHandler ValueChanged;

        /// <summary>
        /// Virtual method to invoke the ValueChanged event.
        /// </summary>
        protected virtual void OnValueChanged( EventArgs e )
        {
            // Retrieve instance of the ValueChanged event
            EventHandler valueChanged = ValueChanged;

            // Ensure the instance exists
            if( valueChanged != null )
            {
                // Invoke the event
                ValueChanged( this, e );
            }
        }

        // Types of input available
        public enum InputType { Text, Number, Email, Tel, Multiline, Date, DateTime, Password }

        // Private enums
        private InputType type = InputType.Text;
        private string labelElement = "label";
        private string messageCssClass = "validation-message";
        private string failedCssClass = "invalid";
        private string successCssClass = "valid";
        private string messageDisplay = "block";
        private bool browserValidation = true;
        private bool serverValidation = true;

        /// <summary>
        /// The value of the input.
        /// </summary>
        [
            Bindable( true ),
            Description( "Gets or sets the value of the input." ),
            Category( "Input" ),
            Localizable( true )
        ]
        public string Value
        {
            get
            {
                if( ViewState["Value"] == null )
                {
                    return "";
                }
                else
                {
                    return (string)ViewState["Value"];
                }
            }

            set
            {
                ViewState["Value"] = value;
                OnValueChanged( EventArgs.Empty );
            }
        }

        /// <summary>
        /// Order index for the control when using Mail() in a VertoForm.
        /// </summary>
        [
            Description( "Order index for the control when using Mail() in a VertoForm." ),
            Category( "Input" )
        ]
        public int Order { get; set; }

        /// <summary>
        /// Whether to wrap the control element(s) in a <fieldset> tag.
        /// </summary>
        [
            Description( "Whether to wrap the control element(s) in a <fieldset> tag." ),
            Category( "Input" )
        ]
        public bool Fieldset { get; set; }

        /// <summary>
        /// The CSS class of the control's label element.
        /// </summary>
        [
            Description( "The CSS class of the control's label element." ),
            Category( "Input" )
        ]
        public string LabelCssClass { get; set; }

        /// <summary>
        /// A friendly name for the control (used for emails and other forms of display).
        /// </summary>
        [
            Description( "A friendly name for the control (used for emails and other forms of display)." ),
            Category( "Input" )
        ]
        public string Name { get; set; }

        /// <summary>
        /// A label for the control which will be rendered.
        /// </summary>
        [
            Description( "A label for the control which will be rendered." ),
            Category( "Input" )
        ]
        public string Label { get; set; }

        /// <summary>
        /// The HTML element to be used for the label (e.g. label or span).
        /// </summary>
        [
            Description( "The HTML element to be used for the label (e.g. label or span)." ),
            Category( "Input" )
        ]
        public string LabelElement
        {
            get
            {
                return labelElement;
            }

            set
            {
                labelElement = value;
            }
        }

        /// <summary>
        /// Whether the label will be rendered after the control. Set to true to render beneath the control.
        /// </summary>
        [
            Description( "Whether the label will be rendered after the control. Set to true to render beneath the control." ),
            Category( "Input" ),
            DefaultValue( false )
        ]
        public bool LabelAfter { get; set; }

        /// <summary>
        /// The type of input to display.
        /// </summary>
        [
            Description( "The type of input to display." ),
            Category( "Input" ),
            DefaultValue( InputType.Text )
        ]
        public InputType Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        /// <summary>
        /// Whether to enable native browser validation for the input.
        /// </summary>
        [
            Description( "Whether to enable native browser validation for the input." ),
            Category( "Input" ),
            DefaultValue( true )
        ]
        public bool BrowserValidation
        {
            get
            {
                return browserValidation;
            }

            set
            {
                browserValidation = value;
            }
        }

        /// <summary>
        /// Whether to enable server-side validation for the input.
        /// </summary>
        [
            Description( "Whether to enable server-side validation for the input." ),
            Category( "Input" ),
            DefaultValue( true )
        ]
        public bool ServerValidation
        {
            get
            {
                return serverValidation;
            }

            set
            {
                serverValidation = value;
            }
        }

        /// <summary>
        /// Whether the input is optional.
        /// </summary>
        [
            Description( "Whether the input is optional. Input will still be validated; empty values will be accepted as valid, unless AllowEmpty is set to false on your validation type." ),
            Category( "Input" ),
            DefaultValue( false )
        ]
        public bool Optional { get; set; }

        /// <summary>
        /// The name of the VertoValidation method to use to validate this input.
        /// </summary>
        [
            Description( "The name of the VertoValidation method to use to validate this input." ),
            Category( "Input" )
        ]
        public string ValidateMethod { get; set; }

        /// <summary>
        /// Whether the validation message will be rendered after the control. Set to true to render beneath the control.
        /// </summary>
        [
            Description( "Whether the validation message will be rendered after the control. Set to true to render beneath the control." ),
            Category( "Input" ),
            DefaultValue( false )
        ]
        public bool MessageAfter { get; set; }

        /// <summary>
        /// CSS class to be used for the validation message.
        /// </summary>
        [
            Description( "CSS class to be used for the validation message." ),
            Category( "Input" ),
            DefaultValue( "validation-message" )
        ]
        public string MessageCssClass
        {
            get
            {
                return messageCssClass;
            }

            set
            {
                messageCssClass = value;
            }
        }

        /// <summary>
        /// CSS class added to the input when validation has failed.
        /// </summary>
        [
            Description( "CSS class added to the input when validation has failed." ),
            Category( "Input" ),
            DefaultValue( "invalid" )
        ]
        public string FailedCssClass
        {
            get
            {
                return failedCssClass;
            }

            set
            {
                failedCssClass = value;
            }
        }

        /// <summary>
        /// CSS class added to the input when validation is successful.
        /// </summary>
        [
            Description( "CSS class added to the input when validation is successful." ),
            Category( "Input" ),
            DefaultValue( "valid" )
        ]
        public string SuccessCssClass
        {
            get
            {
                return successCssClass;
            }

            set
            {
                successCssClass = value;
            }
        }

        /// <summary>
        /// CSS display attribute to use when the message is being displayed (e.g. block or inline).
        /// </summary>
        [
            Description( "CSS display attribute to use when the message is being displayed (e.g. block or inline)." ),
            Category( "Input" ),
            DefaultValue( "block" )
        ]
        public string MessageDisplay
        {
            get
            {
                return messageDisplay;
            }

            set
            {
                messageDisplay = value;
            }
        }

        /// <summary>
        /// Name of the PasswordRequirements to use for validating this field when the Type is set to Password.
        /// </summary>
        [
            Description( "Name of the PasswordRequirements to use for validating this field when the Type is set to Password." ),
            Category( "Input" ),
            DefaultValue( "Standard" )
        ]
        public string PasswordRequirements { get; set; }

        /// <summary>
        /// Automatically populated with an appropriate message when the input fails to validate.
        /// </summary>
        [
            Description( "Automatically populated with an appropriate message when the input fails to validate." )
        ]
        public string FailedMessage { get; set; }

        /// <summary>
        /// HTML input placeholder text to display.
        /// </summary>
        [
            Description( "HTML input placeholder text to display." ),
            Category( "Input" )
        ]
        public string Placeholder { get; set; }

        /// <summary>
        /// ID of another VertoInput (in the same VertoForm) whose value must match this field's value in order to validate.
        /// </summary>
        [
            Description( "ID of another VertoInput (in the same VertoForm) whose value must match this field's value in order to validate." ),
            Category( "Input" )
        ]
        public string ConfirmField { get; set; }

        /// <summary>
        /// Clears the value and ViewState of the input.
        /// </summary>
        public void Clear()
        {
            Value = "";
            FailedMessage = "";
            ViewState.Clear();
        }

        // Register the control to require postback before rendering (so ViewState data can be stored without forcing use of UniqueID).
        protected override void OnPreRender( EventArgs e )
        {
            Page.RegisterRequiresPostBack( this );
        }

        // Method for rendering the control in HTML
        public override void RenderControl( HtmlTextWriter writer )
        {
            if( Fieldset ) writer.WriteFullBeginTag( "fieldset" );

            // Render the validation message holder
            if( !MessageAfter ) RenderValidator( writer );

            // Render the control label if required
            if( !LabelAfter ) RenderLabel( writer );

            switch( Type )
            {
                case InputType.Multiline:

                    RenderMultiline( writer );
                    break;

                default:

                    RenderTextBox( writer );
                    break;

            }

            // Render the control label if required
            if( LabelAfter ) RenderLabel( writer );

            // Render the validation message holder
            if( MessageAfter ) RenderValidator( writer );

            base.RenderControl( writer );

            if( Fieldset ) writer.WriteEndTag( "fieldset" );
        }

        // Renders the control as an input
        protected void RenderTextBox( HtmlTextWriter writer )
        {
            // Prepare the ValidationType object for client-side validation
            ValidationType validateType;

            // Attempt to retrieve password requirements
            PasswordRequirements passRequirements = VertoValidation.GetPasswordRequirements( PasswordRequirements );

            // Check if we're using a specific method
            if( !String.IsNullOrWhiteSpace( ValidateMethod ) )
            {
                validateType = VertoValidation.GetValidationType( ValidateMethod );
            }
            else
            {
                // Attempt to retrieve ValidationType for the input
                validateType = VertoValidation.GetValidationType( Type.ToString() );
            }

            // Opening tag
            writer.WriteBeginTag( "input" );

            // Attributes            
            writer.WriteAttribute( "id", ClientID );
            writer.WriteAttribute( "name", ClientID );

            // Add class attribute if we have one
            if( !String.IsNullOrWhiteSpace( CssClass ) )
            {
                writer.WriteAttribute( "class", CssClass );
            }

            if( !String.IsNullOrWhiteSpace( FailedCssClass ) )
            {
                writer.WriteAttribute( "data-fclass", FailedCssClass );
            }

            if( !String.IsNullOrWhiteSpace( SuccessCssClass ) )
            {
                writer.WriteAttribute( "data-sclass", SuccessCssClass );
            }

            if( !String.IsNullOrWhiteSpace( Placeholder ) )
            {
                writer.WriteAttribute( "placeholder", Placeholder );
            }

            if( !String.IsNullOrWhiteSpace( ConfirmField ) )
            {
                // Ensure the specified control exists
                VertoInput confirmFieldControl = (VertoInput)FindControl( ConfirmField );

                if( confirmFieldControl != null )
                {
                    // Store the ClientID as an attribute so we can perform client-side validation
                    writer.WriteAttribute( "data-confirm", confirmFieldControl.ClientID );

                    // Mark the control as a confirm field so it does not render its own validation messages
                    confirmFieldControl.Attributes.Add( "data-is-confirm", "true" );
                }
            }

            if( Optional )
            {
                writer.WriteAttribute( "data-optional", "true" );
            }

            // Write any generic attributes
            foreach( var attr in Attributes.Keys )
            {
                writer.WriteAttribute( attr.ToString(), Attributes[attr.ToString()] );
            }

            // Only write the type if browser validation is enabled
            if( BrowserValidation )
            {
                // Write the native HTML type to allow validation
                writer.WriteAttribute( "type", Type.ToString().ToLower() );

                // See if validation exists for the specified type
                if( validateType != null )
                {
                    // Write the client-side RegEx and description
                    writer.WriteAttribute( "pattern", validateType.RegEx );
                    writer.WriteAttribute( "title", validateType.Description );
                }
                else if( passRequirements != null )
                {
                    // Write client-side password RegEx and description
                    writer.WriteAttribute( "pattern", passRequirements.GetRegEx() );
                    writer.WriteAttribute( "title", passRequirements.Description );
                }
            }
            else
            {
                // Make it a standard text input to disable browser validation
                writer.WriteAttribute( "type", "text" );
            }

            // Only write value if we have it
            if( Value != null )
            {
                writer.WriteAttribute( "value", Value.ToString() );
            }

            // End of self-closing tag
            writer.Write( "/>" );
        }

        // Renders the control as a textarea
        protected void RenderMultiline( HtmlTextWriter writer )
        {
            // Prepare the ValidationType object for client-side validation
            ValidationType validateType;

            // Check if we're using a specific method
            if( !String.IsNullOrWhiteSpace( ValidateMethod ) )
            {
                validateType = VertoValidation.GetValidationType( ValidateMethod );
            }
            else
            {
                // Attempt to retrieve ValidationType for the input
                validateType = VertoValidation.GetValidationType( Type.ToString() );
            }

            // Opening tag
            writer.WriteBeginTag( "textarea" );

            // Attributes
            writer.WriteAttribute( "id", ClientID );
            writer.WriteAttribute( "name", ClientID );

            // Add class attribute if we have one
            if( !String.IsNullOrWhiteSpace( CssClass ) )
            {
                writer.WriteAttribute( "class", CssClass );
            }

            if( !String.IsNullOrWhiteSpace( FailedCssClass ) )
            {
                writer.WriteAttribute( "data-fclass", FailedCssClass );
            }

            if( !String.IsNullOrWhiteSpace( Placeholder ) )
            {
                writer.WriteAttribute( "placeholder", Placeholder );
            }

            // Only write the type if browser validation is enabled
            if( BrowserValidation )
            {
                // See if validation exists for the specified type
                if( validateType != null )
                {
                    // Write the client-side RegEx and description
                    writer.WriteAttribute( "pattern", validateType.RegEx );
                    writer.WriteAttribute( "title", validateType.Description );
                }
            }

            // Write any generic attributes
            foreach( var attr in Attributes.Keys )
            {
                writer.WriteAttribute( attr.ToString(), Attributes[attr.ToString()] );
            }

            // End of opening tag
            writer.Write( ">" );

            // Only write value if we have it
            if( Value != null )
            {
                writer.Write( Value.ToString() );
            }

            // End tag
            writer.WriteEndTag( "textarea" );
        }

        // Renders a validation message holder for the control
        protected void RenderValidator( HtmlTextWriter writer )
        {
            // Prepare the ValidationType object for client-side validation
            ValidationType validateType;

            // Set default password requirements if required
            if( Type == InputType.Password && String.IsNullOrWhiteSpace( PasswordRequirements ) )
            {
                PasswordRequirements = "Standard";
            }

            // Attempt to retrieve password requirements object
            PasswordRequirements passRequirements = VertoValidation.GetPasswordRequirements( PasswordRequirements );

            // Check if we're using a specific method
            if( !String.IsNullOrWhiteSpace( ValidateMethod ) )
            {
                validateType = VertoValidation.GetValidationType( ValidateMethod );
            }
            else
            {
                // Attempt to retrieve ValidationType for the input
                validateType = VertoValidation.GetValidationType( Type.ToString() );
            }

            // Only render if there is a validator
            if( validateType != null || ( passRequirements != null && Type == InputType.Password ) || ( !Optional && String.IsNullOrWhiteSpace( Value ) ) )
            {
                // Opening tag
                writer.WriteBeginTag( "span" );

                // Attributes
                writer.WriteAttribute( "data-input", ClientID );
                writer.WriteAttribute( "data-display", MessageDisplay );
                writer.WriteAttribute( "class", MessageCssClass );

                if( Attributes["data-is-confirm"] == "true" )
                {
                    writer.WriteAttribute( "data-is-confirm", "true" );
                }

                // If failed message has been set, this is most likely postback after a failed validation; display the message
                if( !String.IsNullOrWhiteSpace( FailedMessage ) )
                {
                    writer.WriteAttribute( "style", "display:" + MessageDisplay + ";" );
                }
                else
                {
                    writer.WriteAttribute( "style", "display:none;" );
                }

                // End of opening tag
                writer.Write( ">" );

                if( Attributes["data-is-confirm"] == "true" )
                {
                    writer.Write( "Please ensure both fields match." );
                }
                else
                {
                    if( validateType != null )
                    {
                        // Write the description if we have it
                        if( !String.IsNullOrWhiteSpace( FailedMessage ) )
                        {
                            writer.Write( FailedMessage );
                        }
                        else if( !String.IsNullOrWhiteSpace( validateType.Description ) )
                        {
                            // Swap out the line breaks for HTML line breaks
                            writer.Write( validateType.Description.Replace( "\n", "<br>" ) );
                        }
                    }
                    else if( passRequirements != null )
                    {
                        if( !String.IsNullOrWhiteSpace( FailedMessage ) )
                        {
                            writer.Write( FailedMessage );
                        }
                        else if( !String.IsNullOrWhiteSpace( passRequirements.Description ) )
                        {
                            writer.Write( passRequirements.Description.Replace( "\n", "<br>" ) );
                        }
                        else
                        {
                            writer.Write( VertoValidation.GetPasswordDescription( passRequirements ) );
                        }
                    }
                    else
                    {
                        writer.Write( FailedMessage );
                    }
                }

                // End tag
                writer.WriteEndTag( "span" );
            }
        }

        // Renders a label for the control
        protected void RenderLabel( HtmlTextWriter writer )
        {
            if( !String.IsNullOrWhiteSpace( Label ) )
            {
                // Opening tag
                writer.WriteBeginTag( labelElement );

                // Attributes
                writer.WriteAttribute( "class", LabelCssClass );

                // Only write "for" attribute if element is valid
                if( labelElement == "label" )
                {
                    writer.WriteAttribute( "for", ClientID );
                }

                // End of opening tag
                writer.Write( ">" );

                // Element content
                writer.Write( Label );

                // End tag
                writer.WriteEndTag( labelElement );
            }
        }

        // Loads ViewState data on postback
        public bool LoadPostData( string postDataKey, NameValueCollection postCollection )
        {
            // Ensure the key matches the control
            if( postDataKey == UniqueID )
            {
                foreach( string key in postCollection.Keys )
                {
                    if( key.ToLower().Contains( ClientID.ToLower() ) )
                    {
                        // Set the control value accordingly
                        Value = postCollection[key];
                    }
                }
            }
            return false;
        }

        public void RaisePostDataChangedEvent()
        {
            OnValueChanged( EventArgs.Empty );
        }

        // Override the RenderBeginTag and RenderEndTag methods to remove the pointless span element created around custom controls
        public override void RenderBeginTag( HtmlTextWriter writer )
        {
            writer.Write( "" );
        }

        public override void RenderEndTag( HtmlTextWriter writer )
        {
            writer.Write( "" );
        }
    }

    /// <summary>
    /// DropDownList for use in a VertoForm.
    /// </summary>
    public class VertoSelect : DropDownList
    {
        // String properties
        private string messageCssClass = "validation-message";
        private string failedCssClass = "invalid";
        private string successCssClass = "valid";
        private string messageDisplay = "block";
        private string failedMessage = "";

        /// <summary>
        /// Order index for the control when using Mail() in a VertoForm.
        /// </summary>
        [
            Description( "Order index for the control when using Mail() in a VertoForm." ),
            Category( "Input" )
        ]
        public int Order { get; set; }

        /// <summary>
        /// Whether the input is optional.
        /// </summary>
        [
            Description( "Whether the input is optional. When this property is false, the control will be considered invalid if SelectedValue is 0 or an empty string." ),
            Category( "Input" ),
            DefaultValue( false )
        ]
        public bool Optional { get; set; }

        /// <summary>
        /// A friendly name for the control (used for emails and other forms of display).
        /// </summary>
        [
            Description( "A friendly name for the control (used for emails and other forms of display)." ),
            Category( "Input" )
        ]
        public string Name { get; set; }

        /// <summary>
        /// Whether to wrap the control element(s) in a <fieldset> tag.
        /// </summary>
        [
            Description( "Whether to wrap the control element(s) in a <fieldset> tag." ),
            Category( "Input" )
        ]
        public bool Fieldset { get; set; }

        /// <summary>
        /// Whether the validation message will be rendered after the control. Set to true to render beneath the control.
        /// </summary>
        [
            Description( "Whether the validation message will be rendered after the control. Set to true to render beneath the control." ),
            Category( "Input" ),
            DefaultValue( false )
        ]
        public bool MessageAfter { get; set; }

        /// <summary>
        /// CSS class to be used for the validation message.
        /// </summary>
        [
            Description( "CSS class to be used for the validation message." ),
            Category( "Input" ),
            DefaultValue( "validation-message" )
        ]
        public string MessageCssClass
        {
            get
            {
                return messageCssClass;
            }

            set
            {
                messageCssClass = value;
            }
        }

        /// <summary>
        /// CSS class added to the input when validation has failed.
        /// </summary>
        [
            Description( "CSS class added to the input when validation has failed." ),
            Category( "Input" ),
            DefaultValue( "invalid" )
        ]
        public string FailedCssClass
        {
            get
            {
                return failedCssClass;
            }

            set
            {
                failedCssClass = value;
            }
        }

        /// <summary>
        /// CSS class added to the input when validation is successful.
        /// </summary>
        [
            Description( "CSS class added to the input when validation is successful." ),
            Category( "Input" ),
            DefaultValue( "valid" )
        ]
        public string SuccessCssClass
        {
            get
            {
                return successCssClass;
            }

            set
            {
                successCssClass = value;
            }
        }

        /// <summary>
        /// CSS display attribute to use when the message is being displayed (e.g. block or inline).
        /// </summary>
        [
            Description( "CSS display attribute to use when the message is being displayed (e.g. block or inline)." ),
            Category( "Input" ),
            DefaultValue( "block" )
        ]
        public string MessageDisplay
        {
            get
            {
                return messageDisplay;
            }

            set
            {
                messageDisplay = value;
            }
        }

        /// <summary>
        /// Automatically populated with an appropriate message when the input fails to validate.
        /// </summary>
        [
            Description( "Automatically populated with an appropriate message when the input fails to validate." )
        ]
        public string FailedMessage
        {
            get
            {
                return failedMessage;
            }

            set
            {
                failedMessage = value;
            }
        }

        /// <summary>
        /// Clears the value and ViewState of the select.
        /// </summary>
        public void Clear()
        {
            SelectedIndex = 0;
            FailedMessage = "";
            ViewState.Clear();
        }

        protected override void AddAttributesToRender( HtmlTextWriter writer )
        {
            // Store optional value in attribute for client-side validation
            if( Optional ) writer.AddAttribute( "data-optional", "true" );

            if( !String.IsNullOrWhiteSpace( FailedCssClass ) )
            {
                writer.AddAttribute( "data-fclass", FailedCssClass );
            }

            if( !String.IsNullOrWhiteSpace( SuccessCssClass ) )
            {
                writer.AddAttribute( "data-sclass", SuccessCssClass );
            }

            base.AddAttributesToRender( writer );
        }

        public override void RenderBeginTag( HtmlTextWriter writer )
        {
            if( Fieldset ) writer.WriteFullBeginTag( "fieldset" );

            // Render the validation message before the tag if desired
            if( !MessageAfter ) { RenderValidator( writer ); }

            base.RenderBeginTag( writer );
        }

        public override void RenderEndTag( HtmlTextWriter writer )
        {
            base.RenderEndTag( writer );

            // Render the validation message after the tag if desired
            if( MessageAfter ) { RenderValidator( writer ); }

            if( Fieldset ) writer.WriteEndTag( "fieldset" );
        }

        // Renders a validation message holder for the control
        protected void RenderValidator( HtmlTextWriter writer )
        {
            if( !Optional )
            {
                // Opening tag
                writer.WriteBeginTag( "span" );

                // Attributes
                writer.WriteAttribute( "data-vinput", ClientID );
                writer.WriteAttribute( "data-display", MessageDisplay );
                writer.WriteAttribute( "class", MessageCssClass );

                if( !String.IsNullOrWhiteSpace( FailedMessage ) )
                {
                    writer.WriteAttribute( "style", "display:" + MessageDisplay + ";" );
                }
                else
                {
                    writer.WriteAttribute( "style", "display:none;" );
                }

                // End of opening tag
                writer.Write( ">" );

                // Message to display
                writer.Write( "This field is required." );

                // End tag
                writer.WriteEndTag( "span" );
            }
        }
    }

    /// <summary>
    /// CheckBox for use in a VertoForm.
    /// </summary>
    public class VertoCheckBox : CheckBox
    {
        // String properties
        private string messageCssClass = "validation-message";
        private string failedCssClass = "invalid";
        private string successCssClass = "valid";
        private string messageDisplay = "block";
        private string failedMessage = "";

        /// <summary>
        /// Order index for the control when using Mail() in a VertoForm.
        /// </summary>
        [
            Description( "Order index for the control when using Mail() in a VertoForm." ),
            Category( "Input" )
        ]
        public int Order { get; set; }

        /// <summary>
        /// Whether the input is optional.
        /// </summary>
        [
            Description( "Whether the input is optional. When this property is false, the control will be considered invalid if SelectedValue is 0 or an empty string." ),
            Category( "Input" ),
            DefaultValue( false )
        ]
        public bool Optional { get; set; }

        /// <summary>
        /// A friendly name for the control (used for emails and other forms of display).
        /// </summary>
        [
            Description( "A friendly name for the control (used for emails and other forms of display)." ),
            Category( "Input" )
        ]
        public string Name { get; set; }

        /// <summary>
        /// Whether to wrap the control element(s) in a <fieldset> tag.
        /// </summary>
        [
            Description( "Whether to wrap the control element(s) in a <fieldset> tag." ),
            Category( "Input" )
        ]
        public bool Fieldset { get; set; }

        /// <summary>
        /// Whether the validation message will be rendered after the control. Set to true to render beneath the control.
        /// </summary>
        [
            Description( "Whether the validation message will be rendered after the control. Set to true to render beneath the control." ),
            Category( "Input" ),
            DefaultValue( false )
        ]
        public bool MessageAfter { get; set; }

        /// <summary>
        /// CSS class to be used for the validation message.
        /// </summary>
        [
            Description( "CSS class to be used for the validation message." ),
            Category( "Input" ),
            DefaultValue( "validation-message" )
        ]
        public string MessageCssClass
        {
            get
            {
                return messageCssClass;
            }

            set
            {
                messageCssClass = value;
            }
        }

        /// <summary>
        /// CSS class added to the input when validation has failed.
        /// </summary>
        [
            Description( "CSS class added to the input when validation has failed." ),
            Category( "Input" ),
            DefaultValue( "invalid" )
        ]
        public string FailedCssClass
        {
            get
            {
                return failedCssClass;
            }

            set
            {
                failedCssClass = value;
            }
        }

        /// <summary>
        /// CSS class added to the input when validation is successful.
        /// </summary>
        [
            Description( "CSS class added to the input when validation is successful." ),
            Category( "Input" ),
            DefaultValue( "valid" )
        ]
        public string SuccessCssClass
        {
            get
            {
                return successCssClass;
            }

            set
            {
                successCssClass = value;
            }
        }

        /// <summary>
        /// CSS display attribute to use when the message is being displayed (e.g. block or inline).
        /// </summary>
        [
            Description( "CSS display attribute to use when the message is being displayed (e.g. block or inline)." ),
            Category( "Input" ),
            DefaultValue( "block" )
        ]
        public string MessageDisplay
        {
            get
            {
                return messageDisplay;
            }

            set
            {
                messageDisplay = value;
            }
        }

        /// <summary>
        /// Automatically populated with an appropriate message when the input fails to validate.
        /// </summary>
        [
            Description( "Automatically populated with an appropriate message when the input fails to validate." )
        ]
        public string FailedMessage
        {
            get
            {
                return failedMessage;
            }

            set
            {
                failedMessage = value;
            }
        }

        /// <summary>
        /// Clears the value and ViewState of the select.
        /// </summary>
        public void Clear()
        {
            Checked = false;
            FailedMessage = "";
            ViewState.Clear();
        }

        protected override void AddAttributesToRender( HtmlTextWriter writer )
        {
            // Store optional value in attribute for client-side validation
            if( Optional ) InputAttributes.Add( "data-optional", "true" );

            if( !String.IsNullOrWhiteSpace( FailedCssClass ) )
            {
                InputAttributes.Add( "data-fclass", FailedCssClass );
            }

            if( !String.IsNullOrWhiteSpace( SuccessCssClass ) )
            {
                InputAttributes.Add( "data-sclass", SuccessCssClass );
            }

            base.AddAttributesToRender( writer );
        }

        public override void RenderControl( HtmlTextWriter writer )
        {
            if( Fieldset ) writer.WriteFullBeginTag( "fieldset" );

            // Render the validation message before the tag if desired
            if( !MessageAfter ) { RenderValidator( writer ); }

            base.RenderControl( writer );

            // Render the validation message after the tag if desired
            if( MessageAfter ) { RenderValidator( writer ); }

            if( Fieldset ) writer.WriteEndTag( "fieldset" );
        }

        // Renders a validation message holder for the control
        protected void RenderValidator( HtmlTextWriter writer )
        {
            if( !Optional )
            {
                // Opening tag
                writer.WriteBeginTag( "span" );

                // Attributes
                writer.WriteAttribute( "data-vinput", ClientID );
                writer.WriteAttribute( "data-display", MessageDisplay );
                writer.WriteAttribute( "class", MessageCssClass );

                if( !String.IsNullOrWhiteSpace( FailedMessage ) )
                {
                    writer.WriteAttribute( "style", "display:" + MessageDisplay + ";" );
                }
                else
                {
                    writer.WriteAttribute( "style", "display:none;" );
                }

                // End of opening tag
                writer.Write( ">" );

                // Message to display
                writer.Write( "This field is required." );

                // End tag
                writer.WriteEndTag( "span" );
            }
        }
    }


    /// <summary>
    /// A type of validation to be used by the VertoValidation class.
    /// </summary>
    public class ValidationType
    {
        public enum Mode { RegEx, Callback };

        /// <summary>
        /// The unique name of the ValidationMethod.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// User-facing description of the validation requirements.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Regular Expression to use for validation in RegEx Mode.
        /// </summary>
        public string RegEx { get; set; }

        /// <summary>
        /// Callback function to use for validation when in Callback Mode. Must be static and return a boolean.
        /// </summary>
        public Func<string, bool> Callback { get; set; }

        private Mode validationMode = Mode.RegEx;

        /// <summary>
        /// Defines which technique to use for validation.
        /// </summary>
        [DefaultValue( Mode.RegEx )]
        public Mode ValidationMode
        {
            get
            {
                return validationMode;
            }

            set
            {
                validationMode = value;
            }
        }

        private bool ignoreCase = true;

        /// <summary>
        /// Whether to perform case-insensitive validation.
        /// </summary>
        [DefaultValue( true )]
        public bool IgnoreCase
        {
            get
            {
                return ignoreCase;
            }

            set
            {
                ignoreCase = value;
            }
        }
    }

    /// <summary>
    /// Requirements for a password's strength, to use for validation.
    /// </summary>
    public class PasswordRequirements
    {
        private string description;
        private int minLength = 8;
        private int numbersRequired = 0;
        private int specialCharsRequired = 0;

        /// <summary>
        /// Unique name for the password requirements.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// User-facing description of the password requirements.
        /// </summary>
        public string Description
        {
            get
            {
                if( !String.IsNullOrWhiteSpace( description ) )
                {
                    return description;
                }
                else
                {
                    return VertoValidation.GetPasswordDescription( this );
                }
            }

            set
            {
                description = value;
            }
        }

        /// <summary>
        /// Minimum total number of characters required for the password.
        /// </summary>
        [DefaultValue( 8 )]
        public int MinLength
        {
            get
            {
                return minLength;
            }

            set
            {
                minLength = value;
            }
        }

        /// <summary>
        /// Number of numerical characters (0-9) which must be used in the password.
        /// </summary>
        [DefaultValue( 0 )]
        public int NumbersRequired
        {
            get
            {
                return numbersRequired;
            }

            set
            {
                numbersRequired = value;
            }
        }

        /// <summary>
        /// Number of special characters which must be used in the password.
        /// </summary>
        [DefaultValue( 0 )]
        public int SpecialCharsRequired
        {
            get
            {
                return specialCharsRequired;
            }

            set
            {
                specialCharsRequired = value;
            }
        }

        /// <summary>
        /// Generates a RegEx pattern which will validate this password requirements.
        /// </summary>
        /// <returns>RegEx validation pattern as a string.</returns>
        public string GetRegEx()
        {
            return @"^.*(?=.{/a/,})(?=.*\d{/b/,})(?=.*[A-Za-z])(?=.*[^A-Za-z0-9]{/c/,}).*$".Replace( "/a/", MinLength.ToString() ).Replace( "/b/", NumbersRequired.ToString() ).Replace( "/c/", SpecialCharsRequired.ToString() );
        }
    }

    /// <summary>
    /// Functions for validating form input.
    /// </summary>
    public static class VertoValidation
    {
        /// <summary>
        /// Internal list of ValidationType objects to use for validation.
        /// </summary>
        private static List<ValidationType> validationTypes = new List<ValidationType>()
        {
            new ValidationType
            {
                Name = "Name",
                Description = "Please enter a valid name.\nMay only contain letters, spaces and hypens (-).",
                RegEx = @"^([A-z]+[ -]?[A-z])+$",
                IgnoreCase = true
            },
            new ValidationType
            {
                Name = "Tel",
                Description = "Please enter a valid phone number.\nMay only contain numbers, spaces, country codes (e.g. +44) and hyphens (-).",
                RegEx = @"^([+]?[0-9])*([- ]?[ ]?)([(][0-9]+[)])?([- ]?[0-9]+)+$",
                IgnoreCase = true
            },
            new ValidationType
            {
                Name = "Email",
                Description = "Please enter a valid email address.",
                RegEx = @"^((([!#$%&'*+\-/=?^_`{|}~\w])|([!#$%&'*+\-/=?^_`{|}~\w][!#$%&'*+\-/=?^_`{|}~\.\w]{0,}[!#$%&'*+\-/=?^_`{|}~\w]))[@]\w+([-.]\w+)*\.\w+([-.]\w+)*)$",
                IgnoreCase = true
            },
            new ValidationType
            {
                Name = "DateTime",
                Description = "Please enter a valid date.",
                ValidationMode = ValidationType.Mode.Callback,
                Callback = ValidateDateTime
            },
            new ValidationType
            {
                Name = "Number",
                Description = "Please enter only numbers (0-9) and periods (.).",
                RegEx = "^([0-9]*[.]?[0-9]*)$"
            }
        };

        /// <summary>
        /// Internal list of PasswordRequirements objects to use for password validation.
        /// </summary>
        private static List<PasswordRequirements> passwordRequirements = new List<PasswordRequirements>()
        {
            new PasswordRequirements
            {
                Name = "Basic",
                MinLength = 4
            },
            new PasswordRequirements
            {
                Name = "Standard",
                MinLength = 8
            },
            new PasswordRequirements
            {
                Name = "Strong",
                MinLength = 12,
                NumbersRequired = 1
            },
            new PasswordRequirements
            {
                Name = "Extra Strong",
                MinLength = 16,
                NumbersRequired = 1,
                SpecialCharsRequired = 1
            }
        };

        /// <summary>
        /// Returns a list of available ValidationType objects.
        /// </summary>
        /// <returns>List of available ValidationType objects.</returns>
        public static List<ValidationType> AvailableTypes()
        {
            return validationTypes;
        }

        /// <summary>
        /// Adds a new PasswordRequirements object to the class.
        /// </summary>
        /// <param name="Name">A unique name for the PasswordRequirements.</param>
        /// <param name="Description">User-facing description of the password requirements.</param>
        /// <param name="MinLength">Minimum total number of characters the password must use.</param>
        /// <param name="NumbersRequired">Minimum number of numerical characters (0-9) the password must use.</param>
        /// <param name="SpecialCharsRequired">Minimum number of special characters the password must use.</param>
        public static void AddPasswordRequirements( string Name, string Description = "", int MinLength = 1, int NumbersRequired = 0, int SpecialCharsRequired = 0 )
        {
            // Only add if a PasswordRequirements object with the same name is not already present
            if( passwordRequirements.Find( requirements => requirements.Name.ToLower() == Name.ToLower() ) == null )
            {
                // Prepare a new PasswordRequirements object
                PasswordRequirements newRequirements = new PasswordRequirements()
                { Name = Name, Description = Description, MinLength = MinLength, NumbersRequired = NumbersRequired, SpecialCharsRequired = SpecialCharsRequired };

                // Add the new object to the internal collection
                passwordRequirements.Add( newRequirements );
            }
        }

        /// <summary>
        /// Adds a new ValidationType to the class.
        /// </summary>
        /// <param name="Name">A unique name for the ValidationType.</param>
        /// <param name="Description">The user-facing description for the ValidationType (displayed in a tooltip, and also when validation fails).</param>
        /// <param name="IgnoreCase">Whether to perform case-insensitive validation.</param>
        /// <param name="Mode">The ValidationMode to use.</param>
        /// <param name="RegEx">Regular Expression to use for validation in RegEx Mode.</param>
        /// <param name="Callback">Static callback function to use for validation when in Callback Mode. Must accept a string and return a boolean.</param>
        public static void AddType( string Name, string Description = "", ValidationType.Mode Mode = ValidationType.Mode.RegEx, bool IgnoreCase = true, string RegEx = "", Func<string, bool> Callback = null )
        {
            // Only add if a type with the specified Name does not already exist
            if( validationTypes.Find( type => type.Name.ToLower() == Name.ToLower() ) == null )
            {
                // Prepare a new ValidationType object
                ValidationType newType = new ValidationType()
                { Name = Name, Description = Description, ValidationMode = Mode, RegEx = RegEx, Callback = Callback, IgnoreCase = IgnoreCase };

                // Add the new object to the internal collection
                validationTypes.Add( newType );
            }
        }

        /// <summary>
        /// Adds a new ValidationType to the class.
        /// </summary>
        /// <param name="Type">ValidationType object to add to the class.</param>
        public static void AddType( ValidationType Type )
        {
            // Only add if a type with the same name is not already present
            if( validationTypes.Find( type => type.Name.ToLower() == Type.Name.ToLower() ) == null )
            {
                validationTypes.Add( Type );
            }
        }

        /// <summary>
        /// Adds a new PasswordRequirements object to the class.
        /// </summary>
        /// <param name="Requirements">PasswordRequirements object to add to the class.</param>
        public static void AddPasswordRequirements( PasswordRequirements Requirements )
        {
            // Only add if a PasswordRequirements object with the same name is not already present
            if( passwordRequirements.Find( requirements => requirements.Name.ToLower() == Requirements.Name.ToLower() ) == null )
            {
                passwordRequirements.Add( Requirements );
            }
        }

        /// <summary>
        /// Returns the specified ValidationType, if it exists.
        /// </summary>
        /// <param name="Name">Name of the ValidationType to find.</param>
        /// <returns>The ValidationType if found, otherwise null.</returns>
        public static ValidationType GetValidationType( string Name )
        {
            try
            {
                return validationTypes.Find( type => type.Name.ToLower() == Name.ToLower() );
            }
            catch( Exception )
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the specified PasswordRequirements, if it exists.
        /// </summary>
        /// <param name="Name">Name of the PasswordRequirements to find.</param>
        /// <returns>The PasswordRequirements if found, otherwise null.</returns>
        public static PasswordRequirements GetPasswordRequirements( string Name )
        {
            try
            {
                return passwordRequirements.Find( requirements => requirements.Name.ToLower() == Name.ToLower() );
            }
            catch( Exception )
            {
                return null;
            }
        }

        /// <summary>
        /// Generates a description for a password field based on its requirements.
        /// </summary>
        /// <param name="Requirements">PasswordRequirements object to base the description on.</param>
        /// <returns>Description of the PasswordRequirements suitable for display.</returns>
        public static string GetPasswordDescription( PasswordRequirements Requirements )
        {
            if( Requirements != null )
            {
                // There will always be a minimum length
                string output = "Password must be at least " + Requirements.MinLength.ToString() + " characters long";

                // Check if we need to append message about numbers
                if( Requirements.NumbersRequired > 0 )
                {
                    if( Requirements.NumbersRequired == 1 )
                    {
                        output += " and contain at least 1 number";
                    }
                    else
                    {
                        output += " and contain at least " + Requirements.NumbersRequired.ToString() + " numbers";
                    }

                    // Check if we need to append message about special characters
                    if( Requirements.SpecialCharsRequired > 0 )
                    {
                        if( Requirements.SpecialCharsRequired == 1 )
                        {
                            output += " and 1 special character.";
                        }
                        else
                        {
                            output += " and " + Requirements.SpecialCharsRequired.ToString() + " special characters.";
                        }
                    }
                    else
                    {
                        output += ".";
                    }
                }
                else if( Requirements.SpecialCharsRequired > 0 )
                {
                    if( Requirements.SpecialCharsRequired == 1 )
                    {
                        output += " and contain at least 1 special character.";
                    }
                    else
                    {
                        output += " and contain at least " + Requirements.SpecialCharsRequired.ToString() + " special characters.";
                    }
                }
                else
                {
                    output += ".";
                }

                return output;
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Validates the provided string using the specified validation method.
        /// </summary>
        /// <param name="Input">String to validate.</param>
        /// <param name="TypeName">Name of the ValidationType to use for validation.</param>
        /// <returns></returns>
        public static bool Validate( string Input, string TypeName, bool Optional = false )
        {
            // Attempt to find the specified ValidationType
            ValidationType validateType = validationTypes.Find( type => type.Name.ToLower() == TypeName.ToLower() );

            // Check if the specified type exists
            if( validateType != null )
            {
                // Return false if input is empty and field is required
                if( String.IsNullOrWhiteSpace( Input ) )
                {
                    if( Optional )
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                // Validate according to the appropriate type
                if( validateType.ValidationMode == ValidationType.Mode.RegEx )
                {
                    // Ensure there is a RegEx available
                    if( !String.IsNullOrWhiteSpace( validateType.RegEx ) )
                    {
                        // Adjust flags depending on whether we're being case-insensitive
                        if( validateType.IgnoreCase )
                        {
                            return Regex.IsMatch( Input, validateType.RegEx, RegexOptions.IgnoreCase );
                        }
                        else
                        {
                            return Regex.IsMatch( Input, validateType.RegEx );
                        }
                    }
                    else
                    {
                        // Missing RegEx
                        return false;
                    }
                }
                else
                {
                    if( validateType.Callback != null )
                    {
                        // Run the validation method and return its result
                        return validateType.Callback.Invoke( Input );
                    }
                    else
                    {
                        // Invalid method
                        return false;
                    }
                }
            }
            else
            {
                throw new MissingValidationTypeException( "A ValidationType with the name " + TypeName + " does not exist." );
            }
        }

        /// <summary>
        /// Validates the provided string using the specified password requirements.
        /// </summary>
        /// <param name="Input">String to validate.</param>
        /// <param name="Requirements">Name of the PasswordRequirements object to use for validation.</param>
        /// <returns></returns>
        public static bool ValidatePassword( string Input, string Requirements )
        {
            if( !String.IsNullOrWhiteSpace( Input ) )
            {
                // Attempt to retrieve PasswordRequirements object
                PasswordRequirements passRequirements = GetPasswordRequirements( Requirements );

                // Ensure the PasswordRequirements object exists
                if( passRequirements != null )
                {
                    // Prepare the regular expression with the appropriate variables
                    string regExPattern = @"^.*(?=.{/a/,})(?=.*\d{/b/,})(?=.*[A-Za-z])(?=.*[^A-Za-z0-9]{/c/,}).*$".Replace(
                        "/a/", passRequirements.MinLength.ToString() ).Replace(
                        "/b/", passRequirements.NumbersRequired.ToString() ).Replace(
                        "/c/", passRequirements.SpecialCharsRequired.ToString() );

                    return Regex.IsMatch( Input, regExPattern, RegexOptions.IgnoreCase );
                }
                else
                {
                    throw new MissingPasswordRequirementsException( "A PasswordRequirements object with the name " + Requirements + " does not exist." );
                }
            }
            else
            {
                // Blank password
                return false;
            }
        }

        /// <summary>
        /// Checks if provided input is a valid DateTime string.
        /// </summary>
        private static bool ValidateDateTime( string Input )
        {
            DateTime tempDateTime;

            if( DateTime.TryParse( Input, out tempDateTime ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// Exception thrown when a ValidationType is not found.
    /// </summary>
    [Serializable]
    public class MissingValidationTypeException : Exception
    {
        public MissingValidationTypeException() { }
        public MissingValidationTypeException( string message ) : base( message ) { }
        public MissingValidationTypeException( string message, Exception inner ) : base( message, inner ) { }
        protected MissingValidationTypeException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context ) : base( info, context ) { }
    }

    /// <summary>
    /// Exception thrown when a PasswordRequirements is not found.
    /// </summary>
    [Serializable]
    public class MissingPasswordRequirementsException : Exception
    {
        public MissingPasswordRequirementsException() { }
        public MissingPasswordRequirementsException( string message ) : base( message ) { }
        public MissingPasswordRequirementsException( string message, Exception inner ) : base( message, inner ) { }
        protected MissingPasswordRequirementsException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context ) : base( info, context ) { }
    }
}